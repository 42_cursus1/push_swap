/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/05 18:07:54 by acloos            #+#    #+#             */
/*   Updated: 2023/02/15 13:04:09 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include<unistd.h>
# include<stdlib.h>
# include<limits.h>
# include "libft.h"
# include "ansi_colors.h"

/* typedef struct s_swap
{
*next
stack_size	size of stack X
x			value of the node
rank		cf CQ index
curpos		current position in stack X
tarpos		target position in stack A (used only for stack B)
cost_a		how many actions it would cost to rotate stack A
			so that the element at the target position gets to the top of stack A
cost_b		how many actions it would cost to rotate stack B so that this element
			gets to the top of stack B
}	t_swap; */

typedef struct s_swap
{
	struct s_swap	*next;
	int				stack_size;
	int				x;
	int				rank;
	int				curpos;
	int				tarpos;
	int				cost_a;
	int				cost_b;
}	t_swap;

//push_swap
void	check_dup(int ac, char **av);
void	start_ps(t_swap **stack_a, t_swap **stack_b);
int		nb_cmp(char *str1, char *str2);
int		ft_iszero(char *str);
int		check_args(int argc, char **argv);

// utils
void	ft_error(void);
void	check_val(const char *value);
int		ps_atoi(const char *nptr);
int		cost_to_pos(int cost);

// linked lists utils
void	free_ll(t_swap **root);
void	insert_start(t_swap **root, int value);
void	insert_end(t_swap **root, int value);
int		ps_lstsize(t_swap *lst);
void	fill_stacka(t_swap **stack_a, char *value);

// sort small
void	for_two(t_swap **stack_a);
void	ra_three(int tmp, t_swap **stack_a);
void	rra_three(int tmp, t_swap **stack_a);
void	for_three(t_swap **stack_a);
void	sort_five(t_swap **stack_a, t_swap **stack_b, int size);

// sort large
void	check_order(t_swap **stack_a);
void	get_rank(t_swap **stack_a, int size);
void	for_four(t_swap **stack_a, t_swap **stack_b);
void	push_to_b(t_swap **stack_a, t_swap **stack_b, int size);
int		unsorted(t_swap **stack_a, t_swap **stack_b);

// positioning
void	get_curpos(t_swap **stack);
void	get_tarpos(t_swap **stack_a, t_swap **stack_b);
void	get_cost(t_swap **stack_a, t_swap **stack_b, int size_a, int size_b);
int		right_pos(t_swap *tmpa, t_swap *tmpb);

// moving
void	do_move(t_swap **stack_a, t_swap **stack_b, int ca, int cb);
void	move_a(t_swap **stack_a, int *ca);
void	move_b(t_swap **stack_b, int *cb);
void	choose_node(t_swap **stack_a, t_swap **stack_b);
void	get_to_move(t_swap **stack_a, t_swap **stack_b);

// push_swap moves
// push fct: pb & pa
void	push_pa(t_swap **stack_a, t_swap **stack_b);
void	push_pb(t_swap **stack_a, t_swap **stack_b);

//swap fct: sa & sb & ss
void	swap_sa(t_swap **stack_a);
void	swap_sb(t_swap **stack_b);
void	swap_ss(t_swap **stack_a, t_swap **stack_b);

// rotate fct: ra & rb & rr
void	rotate_stack(t_swap **stack);
void	rotate_ra(t_swap **stack_a);
void	rotate_rb(t_swap **stack_b);
void	rotate_rr(t_swap **stack_a, t_swap **stack_b);
t_swap	*get_end(t_swap *stack_x);

// reverse rotate fct: ra & rb & rr
void	rev_stack(t_swap **stack);
void	rev_ra(t_swap **stack_a);
void	rev_rb(t_swap **stack_b);
void	rev_rr(t_swap **stack_a, t_swap **stack_b);
t_swap	*get_antep(t_swap *stack_x);

#endif
