/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   todo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/20 08:54:30 by acloos            #+#    #+#             */
/*   Updated: 2023/02/09 11:17:49 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

MAKEFILE:
check use of cflags: -m -mmd -mp
-> man gcc

TODO:
step_1 : check error cases (only ints, not exceeding integer limits, no duplicates)
	if no error, go to step_2
step_2 : check if list is already sorted
	if not sorted, got to step_3
step_3 : check the case that applies :
			- 2 values
			- 3 values
			- 4 values or more


case_1 : 2 values, unsorted => step = sa
case_2 : 3 values
case_3 : 4+ values

error/special cases
	- no argument given -> do not do anything
	- some args are not int			   _
	- some args are bigger than int		| -> ft_printf("Error\n")
	- there are duplicates				|	  
	- etc							   _|

		errors/cases checked:
			no args / strings & other non-numeric / empty strings
			floats / smaller than intmin / bigger than intmax / duplicates


si 1 seul int, ou int dans l'ordre -> on n'affiche rien


process :
	create fx for each push_swap
		-> sa // sb // ss // pa // pb // ra // rb // rr // rra // rrb // rrr
			-> except : sb // ss


 	declare stack_a & stack_b, stack_size
 
 	if argc > 2
		stack_b = NULL
		fill stack_a with all args
			check values
				if != int
					ft_error
					exit
				if duplicates
					ft_error
					exit
 
 		get stack_size
		if argc == 3
			ordered ? nothing : 'sa';
 		if argc == 4
			algo_3
		if argc > 4
			algo_4
 
 	=> algo_3;
		(see test_pushswap.c)

	=> algo_4:
		push all values minus 3 to stack_b
 		algo_3
		calculate cur_pos & targ_pos (current position & target position)
			find position of each elem in each stack
			find tar_pos in stack_a for elem in stack_b
			scan linked list  and assign a position to each elem
			-> position will be calculates every loop
			-> to find tar_pos, need to find for a given elem
				- which stack_a elem is immediately superior
				- so that stack_b elem can take its place in the list
		calculate cost for mov_a and mov_b
		execute appropriate course of actions
		rotate stack_a to correct position	
 
 
 