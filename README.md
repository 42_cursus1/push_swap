# push_swap

## What the project is about
In this project, we are tasked with creating a program that takes int values and sorts them in ascending order, using as few moves as possible.
We are allowed to use 2 "stacks", and predefined moves, which include push and swap, but also rotate and reverse rotate.
To "push" is to push one element from the top of one stack to the top of the other ; to "swap" is to swap the first two elements of a stack ; to "rotate" is to take the first element of a stack and put it at the bottom of the stack (and the second element becomes the first one, etc) ; to "reverse rotate" is to take the last element of the stack and put it at the top (and the first element becomes the second one, etc).
The idea is to start working with chained lists, and get a deeper understanding of pointers.


## How I approached it
One key element of the project rules is that we want to use as few moves as possible. However, running through our stacks are not "moves" per se (or at the very least, per the rules). So I went with a "positional" approach : basically I ran through my stacks several times in order to collect data, then I used this data to find out which element I should move, and where.

This is the summed up process:
- check arguments
- add all values to stack A
- push all values to stack B, except 3 (the smallest, the largest, and... another random one). Then sort these 3
- compute which element I should move from B to A, then move it at the desire position
- repeat above step until all elements are back to A, sorted


Now let's go through the process, step by step.
Once I have checked that every argument fed as input to the program is considered valid (basically: all of them are numeric values that fit into an int, and there are no duplicates), I stock them in a linked list as individual nodes. This list will be my stack "A". Now I can start collecting some data on each node, and put it into a struct.
What I first need to know is, well, the value of each node. With this info, I can rank them from smallest to largest. And I also need to know the size of my stack.

As I said, we are allowed to use 2 stacks in order to move things around. So now I'll start pushing nodes to stack "B". Basically, I want stack "A" to retain the node with the smallest rank and the one with the biggest rank, as all other node will necessarily fit between them. And I want to keep another random one, because up to 3 values is easy enough to sort in just a few steps.
Also, in order to save moves in the end, I will accept to spend a few extra moves on this part by using a pre-sort. This means that I will go through "A" twice: the first time pushing only the smaller nodes to "B", then a second time I will push everything - except for the ones with minimum and maximum rank obviously - until I have only 3 nodes left in "A". Thus, values in B won't be spread as randomly , allowing for fewer moves in the end. In school, we are specifically tested on the number of moves for up to 500 values, and this determines our grade. This pre-sort ensures to remain consistently within range for top grades. Without pre-sorting, I had about 10% odds of using just a few more moves than expected for top grade...

Now we get to the second part of data collection:
How do I know where to push a node from "B" to "A"? Because the B node value/rank is higher than a given A node, and smaller than the next one!
How do I know how many moves it will cost? Because I know the position of each node in each stack.

So this is where the main loop is in the program, with mode data added to the struct:
I need to run through each stack independently, and get the current position of each node.
Then I will run through both of them, to determine where each B node should go (its target position).
And then, depending on whether the node is on the lower or upper half of its stack, I can compute how many times I should rotate or reverse rotate the stack.
This info is only useful for the "B" stack: for each B node, I want to know how many times I need to (reverse) rotate stack "B" to put it on top, and how many times I need to (reverse) rotate "A" so that the A node is the one on top of which I want to push the B node. As a note, the cost will be stored as positive or negative, depending on whether I should rotate or reverse rotate the stack.
In the end, my struct contains the following: value, rank, current position, target position(only used for stack "B"), cost of moving "A", cost of moving "B", and size of each stack.
The total cost of moving a node is the sum of the absolute values of both costs. Therefore the node that will be moved is the one that has the lower total cost; if there are more than one B node with that same cost, I simply move the 1st encountered in the stack "B".
Once the node is pushed from "B" to "A"... back to the beginning of the loop!

Basically, I will use as few moves as possible, but there is A LOT of data collecting/computing done ahead of the moves, which makes it a rather slow program. Well, the goal is to optimize the number of moves, not the time/memory usage.

The loop stops when "B" is NULL, which means that all nodes are back in "A", and should be sorted (if not, it means there is an issue in the loop).
However, if the values are sorted, the pointer to "A" may not point to the smallest node. So the stack may need to be (reverse) rotated until it does.
And, tadaa! That's the end of the program!


## What I learned along the way
The way pointers work in C was still kind of hazy to me, I can proudly say that this not the case anymore - not proclaiming I know and comprehend everything, but at least it's not fuzzy anymore. I know that the radix-sort algorithm is quite popular among other students, as it seems easier and quicker to implement. I probably would have spent less time on this project, had I used it. That means I would have spent a lot less time manipulating pointers, so I feel that I sort of needed to use the positional approach (or any that would make a heavy use of pointers) ; and also, the positional approach felt a lot easier to understand (like, more natural?) and implement by myself, instead on relying on having someone explain the way it works, possibly every step of the way...

Another thing that was pointed out to me during the eval, is how the exit() function works. I already kind of understood that using it to end my program was a bit brutal (even more so with EXIT_FAILURE in case of error), but the way it sends a SIGNAL was explained further. I had already had a slight hunch that I should have used a return statement in some cases, but I was too far ahead in writing my code, and the way I wrote it precluded me from using a return (as the program would have continued anyway, instead of just stopping on error). I'll admit I did not want to rewrite everything from scratch at that point... That's a lesson in code architecture, and I will keep in mind to structure my code so that an error can be handled by a return statement whenever possible.

## How would I make it better / what is missing
Hum... when reviewing my code later on, I accidentally found out that I have a minor leak when int-min or int-max are part of the input... so that's one needed fix.
And there's the code architecture point mentioned above, about handing errors.
Finally, in order to achieve a full 125% on the project, I would need to build a checker myself, but I was hort on time to spend on the project.

For now I don't intend on going back to this project, will see later when I have more time on my hands.
