/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/20 08:55:50 by acloos            #+#    #+#             */
/*   Updated: 2023/02/14 16:09:47 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_error(void)
{
	ft_printf("Error\n");
	exit(EXIT_FAILURE);
}

void	check_val(const char *value)
{
	int	i;

	i = 0;
	if (!value[0] || ft_strlen(value) > 11)
		ft_error();
	if ((value[0] == '+' || value[0] == '-'))
	{
		if (ft_isdigit(value[i + 1]))
			i++;
		else
			ft_error();
	}
	while (value[i])
	{
		if (ft_isdigit(value[i]))
			i++;
		else
			ft_error();
	}
}

int	ps_atoi(const char *nptr)
{
	int			i;
	int			sign;
	long int	res;

	i = 0;
	sign = 1;
	res = 0;
	if (nptr[i] == '-')
	{
		sign *= -1;
		i++;
	}
	else if (nptr[i] == '+')
		i++;
	while (nptr[i] >= '0' && nptr[i] <= '9')
		res = res * 10 + nptr[i++] - '0';
	if (sign * res > 2147483647 || sign * res < -2147483648)
		ft_error();
	return (sign * res);
}

int	cost_to_pos(int cost)
{
	if (cost < 0)
		cost *= -1;
	return (cost);
}
