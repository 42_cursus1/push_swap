/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/20 08:42:03 by acloos            #+#    #+#             */
/*   Updated: 2023/02/10 10:03:52 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_swap	*get_end(t_swap *stack_x)
{
	while (stack_x && stack_x->next != NULL)
	{
		stack_x = stack_x->next;
	}
	return (stack_x);
}

void	rotate_stack(t_swap **stack)
{
	t_swap	*tmp;
	t_swap	*end;

	tmp = *stack;
	*stack = (*stack)->next;
	end = get_end(*stack);
	tmp->next = NULL;
	end->next = tmp;
}

void	rotate_ra(t_swap **stack_a)
{
	rotate_stack(stack_a);
	ft_printf("ra\n");
}

void	rotate_rb(t_swap **stack_b)
{
	rotate_stack(stack_b);
	ft_printf("rb\n");
}

void	rotate_rr(t_swap **stack_a, t_swap **stack_b)
{
	rotate_stack(stack_a);
	rotate_stack(stack_b);
	ft_printf("rr\n");
}
