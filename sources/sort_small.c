/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_small.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/20 12:19:48 by acloos            #+#    #+#             */
/*   Updated: 2023/02/15 13:03:58 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	for_two(t_swap **stack_a)
{
	if ((*stack_a)->x > (*stack_a)->next->x)
		swap_sa(stack_a);
}

void	ra_three(int tmp, t_swap **stack_a)
{
	int		tmpr;
	t_swap	*tmpa;

	tmpa = (*stack_a);
	tmp = tmpa->x;
	tmpr = tmpa->rank;
	tmpa->x = tmpa->next->x;
	tmpa->rank = tmpa->next->rank;
	tmpa->next->x = tmpa->next->next->x;
	tmpa->next->rank = tmpa->next->next->rank;
	tmpa->next->next->x = tmp;
	tmpa->next->next->rank = tmpr;
	ft_printf("ra\n");
}

void	rra_three(int tmp, t_swap **stack_a)
{
	int	tmpr;

	tmp = (*stack_a)->next->x;
	tmpr = (*stack_a)->next->rank;
	(*stack_a)->next->x = (*stack_a)->x;
	(*stack_a)->next->rank = (*stack_a)->rank;
	(*stack_a)->x = (*stack_a)->next->next->x;
	(*stack_a)->rank = (*stack_a)->next->next->rank;
	(*stack_a)->next->next->x = tmp;
	(*stack_a)->next->next->rank = tmpr;
	ft_printf("rra\n");
}

void	for_three(t_swap **stack_a)
{
	int	tmp;

	tmp = (*stack_a)->x;
	while ((*stack_a)->x > (*stack_a)->next->x
		|| (*stack_a)->x > (*stack_a)->next->next->x
		|| (*stack_a)->next->x > (*stack_a)->next->next->x)
	{
		if ((*stack_a)->x > (*stack_a)->next->x
			&& (*stack_a)->x > (*stack_a)->next->next->x)
			ra_three(tmp, &(*stack_a));
		if ((*stack_a)->next->x > (*stack_a)->x
			&& (*stack_a)->next->x > (*stack_a)->next->next->x)
			rra_three(tmp, &(*stack_a));
		if ((*stack_a)->x > (*stack_a)->next->x
			&& (*stack_a)->x < (*stack_a)->next->next->x)
			swap_sa(stack_a);
	}
}

void	sort_five(t_swap **stack_a, t_swap **stack_b, int size)
{
	int	pushed;

	pushed = 0;
	while (size - pushed > 3)
	{
		if ((*stack_a)->rank == 1 || (*stack_a)->rank == size)
			rotate_ra(stack_a);
		else
		{
			push_pb(stack_a, stack_b);
			pushed++;
		}
	}
}
