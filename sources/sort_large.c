/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_large.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/26 15:11:25 by acloos            #+#    #+#             */
/*   Updated: 2023/02/15 13:03:33 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	check_order(t_swap **stack_a)
{
	t_swap			*tmp;

	tmp = (*stack_a);
	while (tmp->next != NULL)
	{
		if (tmp->x > tmp->next->x)
			return ;
		tmp = tmp->next;
	}
	free_ll(stack_a);
	exit(EXIT_SUCCESS);
}

void	get_rank(t_swap **stack_a, int size)
{
	t_swap	*tmp;
	t_swap	*max;
	int		value;

	while (--size > 0)
	{
		tmp = (*stack_a);
		value = -2147483648;
		max = NULL;
		while (tmp != NULL)
		{
			if (tmp->x == -2147483648 && tmp->rank == 0)
				tmp->rank = 1;
			if (tmp->x > value && tmp->rank == 0)
			{
				value = tmp->x;
				max = tmp;
				tmp = (*stack_a);
			}
			else
				tmp = tmp->next;
		}
		if (max != NULL)
			max->rank = size;
	}
}

void	push_to_b(t_swap **stack_a, t_swap **stack_b, int size)
{
	int	pushed;

	pushed = 0;
	while (pushed < size / 3)
	{
		if ((*stack_a)->rank == 1 || (*stack_a)->rank == size)
			rotate_ra(stack_a);
		else if ((*stack_a)->rank <= size / 2 + 1)
		{
			push_pb(stack_a, stack_b);
			pushed++;
		}
		else
			rotate_ra(stack_a);
	}
	while (size - pushed > 3)
	{
		if ((*stack_a)->rank == 1 || (*stack_a)->rank == size)
			rotate_ra(stack_a);
		else
		{
			push_pb(stack_a, stack_b);
			pushed++;
		}
	}
}

int	unsorted(t_swap **stack_a, t_swap **stack_b)
{
	if ((*stack_b) != NULL)
		if ((*stack_b)->stack_size > 0)
			return (1);
	if ((*stack_a)->rank > ps_lstsize(*stack_a) / 2)
	{
		while ((*stack_a)->rank != 1)
			rotate_ra(stack_a);
	}
	else
	{
		while ((*stack_a)->rank != 1)
			rev_ra(stack_a);
	}
	check_order(stack_a);
	return (0);
}

void	for_four(t_swap **stack_a, t_swap **stack_b)
{
	check_order(stack_a);
	get_rank(stack_a, (*stack_a)->stack_size + 1);
	if ((*stack_a)->stack_size == 5)
		sort_five(stack_a, stack_b, (*stack_a)->stack_size);
	else
		push_to_b(stack_a, stack_b, (*stack_a)->stack_size);
	for_three(stack_a);
	while (unsorted(stack_a, stack_b) > 0)
		get_to_move(stack_a, stack_b);
}
