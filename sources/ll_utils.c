/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ll_utils.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/26 11:44:19 by acloos            #+#    #+#             */
/*   Updated: 2023/02/14 16:11:42 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	free_ll(t_swap **root)
{
	t_swap	*current;
	t_swap	*tmp;

	current = *root;
	while (current != NULL)
	{
		tmp = current;
		current = current->next;
		free(tmp);
	}
	*root = NULL;
}

void	insert_end(t_swap **root, int value)
{
	t_swap	*new_node;
	t_swap	*current;

	new_node = malloc(sizeof * new_node);
	if (!new_node)
		ft_error();
	new_node->next = NULL;
	new_node->x = value;
	new_node->rank = 0;
	if (*root == NULL)
	{
		*root = new_node;
		return ;
	}
	current = *root;
	while (current->next != NULL)
		current = current->next;
	current->next = new_node;
}

int	ps_lstsize(t_swap *lst)
{
	t_swap	*elem;
	int		i;

	i = 0;
	elem = lst;
	while (elem != NULL)
	{
		elem = elem -> next;
		i++;
	}
	return (i);
}

void	fill_stacka(t_swap **stack_a, char *value)
{
	insert_end(stack_a, ps_atoi(value));
	(*stack_a)->stack_size = ps_lstsize((*stack_a));
}
