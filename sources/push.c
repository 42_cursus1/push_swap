/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/20 08:41:31 by acloos            #+#    #+#             */
/*   Updated: 2023/02/09 16:18:23 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	push_pa(t_swap **stack_a, t_swap **stack_b)
{
	t_swap	*tmp;

	if (!stack_b)
		ft_error();
	tmp = (*stack_b)->next;
	if (tmp)
	{
		(*stack_b)->next = *stack_a;
		*stack_a = *stack_b;
		*stack_b = tmp;
	}
	else
	{
		(*stack_b)->next = *stack_a;
		*stack_a = *stack_b;
		*stack_b = NULL;
	}
	(*stack_a)->stack_size = ps_lstsize((*stack_a));
	if (*stack_b != NULL)
		(*stack_b)->stack_size = ps_lstsize((*stack_b));
	else
		stack_b = NULL;
	ft_printf("pa\n");
}

void	push_pb(t_swap **stack_a, t_swap **stack_b)
{
	t_swap	*tmp;

	if (!stack_a)
		ft_error();
	tmp = (*stack_a)->next;
	(*stack_a)->next = *stack_b;
	*stack_b = *stack_a;
	*stack_a = tmp;
	(*stack_b)->stack_size = ps_lstsize((*stack_b));
	(*stack_a)->stack_size = ps_lstsize((*stack_a));
	ft_printf("pb\n");
}
