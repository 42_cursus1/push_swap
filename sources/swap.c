/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/20 08:42:07 by acloos            #+#    #+#             */
/*   Updated: 2023/02/09 16:17:23 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	swap_sa(t_swap **stack_a)
{
	int		tmpx;
	int		tmpr;
	t_swap	*tmpa;

	tmpa = (*stack_a);
	tmpx = tmpa->x;
	tmpr = tmpa->rank;
	tmpa->x = tmpa->next->x;
	tmpa->rank = tmpa->next->rank;
		tmpa->next->x = tmpx;
	tmpa->next->rank = tmpr;
	ft_printf("sa\n");
}

void	swap_sb(t_swap **stack_b)
{
	int	tmpx;
	int	tmpr;

	tmpx = (*stack_b)->x;
	tmpr = (*stack_b)->rank;
	(*stack_b)->x = (*stack_b)->next->x;
	(*stack_b)->rank = (*stack_b)->next->rank;
	(*stack_b)->next->x = tmpx;
	(*stack_b)->next->rank = tmpr;
	ft_printf("sb\n");
}

void	swap_ss(t_swap **stack_a, t_swap **stack_b)
{
	swap_sa(stack_a);
	swap_sb(stack_b);
	ft_printf("ss\n");
}
