/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/31 10:30:37 by acloos            #+#    #+#             */
/*   Updated: 2023/02/14 16:13:14 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int	main(int argc, char **argv)
{
	int		i;
	int		args;
	t_swap	*stack_a;
	t_swap	*stack_b;

	stack_a = NULL;
	stack_b = NULL;
	i = 0;
	args = check_args(argc, argv);
	if (args > 1)
	{
		while (++i < argc)
			fill_stacka(&stack_a, argv[i]);
		start_ps(&stack_a, &stack_b);
		free_ll(&stack_a);
		free_ll(&stack_b);
	}
	return (0);
}
