/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   moving.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/01 16:08:13 by acloos            #+#    #+#             */
/*   Updated: 2023/02/10 15:12:28 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	move_a(t_swap **stack_a, int *ca)
{
	if ((*ca) > 0)
	{
		while ((*ca)-- > 0)
			rotate_ra(stack_a);
	}
	else if ((*ca) < 0)
	{
		while ((*ca)++ < 0)
			rev_ra(stack_a);
	}
}

void	move_b(t_swap **stack_b, int *cb)
{
	if ((*cb) > 0)
	{
		while ((*cb)-- > 0)
			rotate_rb(stack_b);
	}
	else if ((*cb) < 0)
	{
		while ((*cb)++ < 0)
			rev_rb(stack_b);
	}
}

void	do_move(t_swap **stack_a, t_swap **stack_b, int ca, int cb)
{
	if (ca > 0 && cb > 0)
	{
		while (ca > 0 && cb > 0)
		{
			rotate_rr(stack_a, stack_b);
			ca--;
			cb--;
		}
	}
	else if (ca < 0 && cb < 0)
	{
		while (ca < 0 && cb < 0)
		{
			rev_rr(stack_a, stack_b);
			ca++;
			cb++;
		}
	}
	if (ca != 0)
		move_a(stack_a, &ca);
	if (cb != 0)
		move_b(stack_b, &cb);
	push_pa(stack_a, stack_b);
}

void	choose_node(t_swap **stack_a, t_swap **stack_b)
{
	t_swap	*tmp;
	int		min_cost;
	int		a_cost;
	int		b_cost;

	tmp = *stack_b;
	min_cost = ps_lstsize((*stack_a)) + ps_lstsize((*stack_b));
	while (tmp)
	{
		if (cost_to_pos(tmp->cost_a) + cost_to_pos(tmp->cost_b) < min_cost)
		{
			min_cost = cost_to_pos(tmp->cost_a) + cost_to_pos(tmp->cost_b);
			a_cost = tmp->cost_a;
			b_cost = tmp->cost_b;
		}
		tmp = tmp->next;
	}
	do_move(stack_a, stack_b, a_cost, b_cost);
}

void	get_to_move(t_swap **stack_a, t_swap **stack_b)
{
	get_curpos(stack_a);
	get_curpos(stack_b);
	get_tarpos(stack_a, stack_b);
	get_cost(stack_a, stack_b, ps_lstsize(*stack_a), ps_lstsize(*stack_b));
	choose_node(stack_a, stack_b);
}
