/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/05 18:06:39 by acloos            #+#    #+#             */
/*   Updated: 2023/02/15 13:00:23 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	start_ps(t_swap **stack_a, t_swap **stack_b)
{
	if ((*stack_a)->stack_size == 2)
		for_two(stack_a);
	else if ((*stack_a)->stack_size == 3)
		for_three(stack_a);
	else
		for_four(stack_a, stack_b);
}

int	nb_cmp(char *str1, char *str2)
{
	int	i;
	int	j;

	i = 0;
	j = i;
	if (str1[i] == '+')
	{
		if (str2[j] != '+')
			i++;
	}
	if (str2[j] == '+')
		j++;
	while (str1[i] && str2[j] && str1[i] == str2[j])
	{
		i++;
		j++;
	}
	return (str1[i] - str2[j]);
}

int	ft_iszero(char *str)
{
	int	i;

	i = 0;
	if ((str[0] == '+' || str[0] == '-') && str[i + 1] == '0')
		i++;
	while (str[i] && str[i] == '0')
		i++;
	if (str[i] != '\0')
		return (0);
	return (1);
}

void	check_dup(int argc, char **argv)
{
	int	i;
	int	j;
	int	zero;

	i = 1;
	zero = 0;
	while (i < argc)
	{
		j = 1;
		while (argv[j])
		{
			if (j != i && nb_cmp(argv[i], argv[j]) == 0)
				ft_error();
			j++;
		}
		zero += ft_iszero(argv[i]);
		i++;
	}
	if (zero > 1)
		ft_error();
}

int	check_args(int argc, char **argv)
{
	int	i;

	i = 1;
	while (i < argc)
	{
		if (ft_strlen(argv[i]) > 11)
			ft_error();
		check_dup(argc, argv);
		check_val(argv[i]);
		i++;
	}
	return (i);
}
