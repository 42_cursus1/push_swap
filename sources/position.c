/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   position.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/31 12:44:04 by acloos            #+#    #+#             */
/*   Updated: 2023/02/09 15:17:14 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	get_curpos(t_swap **stack)
{
	int		i;
	t_swap	*tmp;

	i = 0;
	tmp = (*stack);
	while (tmp != NULL)
	{
		tmp->curpos = i;
		tmp = tmp->next;
		i++;
	}
}

void	get_cost(t_swap **stack_a, t_swap **stack_b, int size_a, int size_b)
{
	t_swap	*tmpa;
	t_swap	*tmpb;
	int		cost;

	tmpb = *stack_b;
	while (tmpb != NULL)
	{
		tmpa = *stack_a;
		{
			while (tmpb->tarpos != tmpa->curpos)
				tmpa = tmpa->next;
			if (tmpa->curpos <= size_a / 2)
				cost = tmpa->curpos;
			else if (tmpa->curpos > size_a / 2)
				cost = tmpa->curpos - size_a;
			tmpb->cost_a = cost;
			if (tmpb->curpos <= size_b / 2)
				cost = tmpb->curpos;
			else if (tmpb->curpos > size_b / 2)
				cost = tmpb->curpos - size_b;
			tmpb->cost_b = cost;
		}
		tmpb = tmpb->next;
	}
}

int	right_pos(t_swap *tmpa, t_swap *tmpb)
{
	if (tmpb->rank > tmpa->rank && tmpb->rank < tmpa->next->rank)
		return (1);
	else if (tmpa->rank > tmpa->next->rank && tmpb->rank < tmpa->next->rank)
		return (1);
	return (0);
}

void	get_tarpos(t_swap **stack_a, t_swap **stack_b)
{
	t_swap	*tmpa;
	t_swap	*tmpb;

	tmpb = *stack_b;
	while (tmpb != NULL)
	{
		tmpa = *stack_a;
		tmpb->tarpos = 0;
		while (tmpa->next != NULL)
		{
			if (right_pos(tmpa, tmpb) == 1 && tmpa->next != NULL)
				tmpb->tarpos = tmpa->curpos + 1;
			tmpa = tmpa->next;
		}
		tmpb = tmpb->next;
	}
}
