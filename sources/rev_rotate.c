/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rev_rotate.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acloos <acloos@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/20 08:41:58 by acloos            #+#    #+#             */
/*   Updated: 2023/02/10 15:10:14 by acloos           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_swap	*get_antep(t_swap *stack_x)
{
	while (stack_x && stack_x->next->next != NULL)
	{
		stack_x = stack_x->next;
	}
	return (stack_x);
}

void	rev_stack(t_swap **stack)
{
	t_swap	*start;
	t_swap	*endish;
	t_swap	*end;

	start = *stack;
	endish = get_antep(*stack);
	end = get_end(*stack);
	*stack = end;
	(*stack)->next = start;
	endish->next = NULL;
}

void	rev_ra(t_swap **stack_a)
{
	rev_stack(stack_a);
	ft_printf("rra\n");
}

void	rev_rb(t_swap **stack_b)
{
	rev_stack(stack_b);
	ft_printf("rrb\n");
}

void	rev_rr(t_swap **stack_a, t_swap **stack_b)
{
	rev_stack(stack_a);
	rev_stack(stack_b);
	ft_printf("rrr\n");
}
