# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Untitled-1                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: acloos <acloos@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/02/02 09:39:27 by acloos            #+#    #+#              #
#    Updated: 2023/02/02 09:39:27 by acloos           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# Description											fct name			filename

## 1 START WITH MAIN.C

declare and assign stack_a and stack_b					n/a
check that we gave at least 1 arg						n/a
checks there are no duplicate in args					check_dup()			push_swap.c
	if there are: error message							ft_error()			utils.c
fills stack_a with args									fill_stacka()		push_swap.c
	fct will check if args are int						ps_atoi				push_swap.c
		if not, error msg								ft_error()			utils.c
as all args pass tests, start the prog					start_ps()			push_swap.c
...			
when everything is over, free stacks					free_ll()			ll_utils.c

## 2 NOW TO START THE PROG WITH PUSH_SWAP.C

steps differ depending on arg number					n/a
	1 arg: nothing (because already sorted)				n/a
	2 args: depends if args are sorted or not			for_two()			sort_small.c
	3 args: sorting for small list						for_three()			sort_small.c
	4 args: sorting for bigger list						for_four()			sort_large.c

## 3 SORTING A SMALL LIST WITH SORT_SMALL.C

2 args: swap if unsorted (else: do nothing)				swap_sa()			swap.c
3 args : 1 or 2 moves, among sa / ra3 / rra3			swap_sa()			swap.c

## 4 SORTING A BIGGER LIST WITH SORT_LARGE.C

check if ordered (sorted). if sorted: end of prog		check_order()		sort_large.c
get rank of each element of stack_a						get_rank()			sort_large.c
push all but 3 elements in B, with some pre-sorting		push_to_b()			sort_large.c
	either push directly to B							push_pb()			push.c
	or move A to keep element, by either				n/a
		rotating stack_a								rotate_ra()			rotate.c
		or by reverse rotate stack_a					rev_ra()			rev_rotate.c
sort stack_a as a small list (cf step 3)				for_three()			sort_small.c
while stack_b exists and stack_a is unordered: move		get_to_move()		moving.c
...

## 5 FIND OUT WHICH ELEMENT(S) TO MOVE WITH MOVING.C

get current position of each element of each stack		get_curpos()		position.c
get target position in A for each element of B			get_tarpos()		position.c
find the moving cost for each element of B				get_cost()			position.c
choose which element to move							choose_node()		moving.c






## DO THE MOVES ACCORDINGLY
for swapping: swap_sa(), swap_sb(), swap_ss()								swap.c
for pushing: push_pa(), push_pb()											push.c
for rotating: rotate_ra(), rotate_rb(), rotate_rr()							rotate.c
for reverse rotating: rev_ra(), rev_rb(), rev_rr()							rev_rotate.c




cas d'erreur : juste un signe negatif
		signe negatif suivi de 0		- 0
		!! 0 -0 +0 sont des doublons !!!