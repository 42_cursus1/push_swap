# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: acloos <acloos@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/01/12 08:09:05 by acloos            #+#    #+#              #
#    Updated: 2023/02/14 11:03:33 by acloos           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME					=	push_swap
CC						=	gcc
CFLAGS					=	-Wall -Wextra -Werror -g3
RM						=	rm -rf

SRCBAZ					=	push_swap utils sort_small ll_utils sort_large push swap rotate rev_rotate moving main position
SRC						=	$(addsuffix .c, $(addprefix sources/, $(SRCBAZ)))

OBJ_DIR					=	obj
OBJ						=	$(SRC:sources/%.c=$(OBJ_DIR)/%.o)

INCLS					=	-I ./includes/

LIBFT_PATH				=	./libft
LIBFT					=	$(LIBFT_PATH)/libft.a
LIBINCL					=	-L libft/ -lft

$(OBJ_DIR)/%.o:			sources/%.c
						$(CC) $(CFLAGS) -I libft/sources -I sources  -c $< -o $@ $(INCLS)

$(OBJ_BONUS_DIR)/%.o:	bonus/%.c
						$(CC) $(CFLAGS) -c $< -o $@ $(LIBINCL)

all:					$(NAME) 
						@echo "\033[32m[Program is ready for use]\033[0m"


$(NAME):				$(LIBFT) $(OBJ_DIR) $(OBJ)
						$(CC) $(CFLAGS) $(OBJ) -o $(NAME) $(LIBINCL)
						@echo "\033[32m[Push_swap created]\033[0m"

$(LIBFT):
						$(MAKE) -C $(LIBFT_PATH) all -s
						@echo "\033[32m[Libft created]\033[0m"

$(OBJ_DIR):
						mkdir -p $(OBJ_DIR)

$(OBJ_BONUS_DIR):
						mkdir -p $(OBJ_BONUS_DIR)

clean:
						$(MAKE) -C $(LIBFT_PATH) clean
						$(RM) $(OBJ_DIR) $(OBJ_BONUS_DIR)
						@echo "\033[33m[Cleaned up]\033[0m"

fclean:					clean
						$(MAKE) -C $(LIBFT_PATH) fclean
						$(RM) $(NAME)
						@echo "\033[33m[Fully cleaned up]\033[0m"

re:						fclean 
						make all
# pour compiler plus vite => ecrire: make -j re

test3:					$(NAME)
						$(eval ARG = $(shell shuf -i 0-50 -n 3))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test5:					$(NAME)
						$(eval ARG = $(shell shuf -i 0-50 -n 5))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test6:					$(NAME)
						$(eval ARG = $(shell shuf -i 0-50 -n 6))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test7:					$(NAME)
						$(eval ARG = $(shell shuf -i 0-50 -n 7))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test8:					$(NAME)
						$(eval ARG = $(shell shuf -i 0-50 -n 8))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test9:					$(NAME)
						$(eval ARG = $(shell shuf -i 0-50 -n 9))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test10:					$(NAME)
						$(eval ARG = $(shell shuf -i 0-1000 -n 10))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test20:					$(NAME)
						$(eval ARG = $(shell shuf -i 0-1000 -n 20))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test50:					$(NAME)
						$(eval ARG = $(shell shuf -i 0-1000 -n 50))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test75:					$(NAME)
						$(eval ARG = $(shell shuf -i 0-1000 -n 75))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test100:				$(NAME)
						$(eval ARG = $(shell shuf -i 0-1000 -n 100))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test500:				$(NAME)
						$(eval ARG = $(shell shuf -i 0-2000 -n 500))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l
						
test600:				$(NAME)
						$(eval ARG = $(shell shuf -i 0-2000 -n 600))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

test1000:				$(NAME)
						$(eval ARG = $(shell shuf -i 0-5000 -n 1000))
						./push_swap $(ARG) | ./checker_linux $(ARG)
						@echo -n "Instructions: "
						@./push_swap $(ARG) | wc -l

.PHONY:	all clean fclean re libft test3 test5 test100 test500 test25 test600 test1000
